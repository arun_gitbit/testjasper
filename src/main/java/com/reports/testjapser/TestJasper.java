/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.testjapser;

import com.sailfin.tprm.setup.ReadingProperty;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author 400221318
 */
public class TestJasper {

    public static void main(String args[]) {

        TestJasper tj = new TestJasper();
        String companyId = "123456";
        String currentYear = "2015-2016";

        String calculationInterval = "Yearly";
        String integratorName = "DowJones";
        Connection connection = null;
        try {
            connection = getPostgreConnection();
        } catch (Exception ex) {
            Logger.getLogger(TestJasper.class.getName()).log(Level.SEVERE, null, ex);
        }

        tj.createPDF(companyId, currentYear, tj.gettingYear(currentYear, 1), tj.gettingYear(currentYear, 2), calculationInterval, integratorName, connection);

    }

    protected static Connection getPostgreConnection() throws Exception {

        Class.forName("org.postgresql.Driver");

        return DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "root");

    }

    protected static void export(JasperPrint jasperPrint, File outputDir) throws Exception {
        JasperExportManager.exportReportToPdfFile(jasperPrint,
                new File(outputDir, jasperPrint.getName() + ".pdf").getPath());

    }

    public byte[] createPDF(String companyId, String currentYear, String lastYear, String secondLastYear, String calculationInterval, String integratorName, Connection connection) {
        byte[] pdfArray = null;
        

        
        try {
            try {
               
               
                //  URL url = new URL("/JasperSources/FRA.jrxml");
                JasperDesign jasperDesignMain = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA.jrxml"));

                JasperReport jasperReportMain = JasperCompileManager.compileReport(jasperDesignMain);
                //Compile Main Report

                JasperDesign fraKeyHighLightSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_KeyHighLights.jrxml"));
                //Compile Main Report
                JasperReport jasperKeyHighlightSubReport = JasperCompileManager.compileReport(fraKeyHighLightSubReport);

                JasperDesign BalanceSheet_CurrentLiabilitiesTableSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/BalanceSheet_CurrentLiabilitiesTable.jrxml"));
                //Compile Main Rep
                JasperReport jasperBalanceSheet_CurrentLiabilitiesTableSubReport = JasperCompileManager.compileReport(BalanceSheet_CurrentLiabilitiesTableSubReport);
                JasperDesign BalanceSheetNonCurrentLiabilitiesTableSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/BalanceSheetNonCurrentLiabilitiesTable.jrxml"));
//			            //Compile Main Report
                JasperReport jasperBalanceSheetNonCurrentLiabilitiesTableSubReport = JasperCompileManager.compileReport(BalanceSheetNonCurrentLiabilitiesTableSubReport);
                JasperDesign FRA_BalanceSheetCurrentAssetDatasetSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_BalanceSheetCurrentAssetDataset.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_BalanceSheetCurrentAssetDatasetSubReport = JasperCompileManager.compileReport(FRA_BalanceSheetCurrentAssetDatasetSubReport);
                JasperDesign FRA_BalanceSheetNonCurrentAssetTableSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_BalanceSheetNonCurrentAssetTable.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_BalanceSheetNonCurrentAssetTableSubReport = JasperCompileManager.compileReport(FRA_BalanceSheetNonCurrentAssetTableSubReport);
                JasperDesign FRA_BalanceSheetShareHolderEquitySubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_BalanceSheetShareHolderEquity.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_BalanceSheetShareHolderEquitySubReport = JasperCompileManager.compileReport(FRA_BalanceSheetShareHolderEquitySubReport);
                JasperDesign FRA_BalanceSheetSupplementItemSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_BalanceSheetSupplementItem.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_BalanceSheetSupplementItemSubReport = JasperCompileManager.compileReport(FRA_BalanceSheetSupplementItemSubReport);
                JasperDesign FRA_BalanceSheetTable1SubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_BalanceSheetTable1.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_BalanceSheetTable1SubReport = JasperCompileManager.compileReport(FRA_BalanceSheetTable1SubReport);
//			           JasperDesign FRA_BalanceSheetTable3SubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_BalanceSheetTable3.jrxml");
//			            //Compile Main Report
//			           JasperReport jasperFRA_BalanceSheetTable3SubReport = JasperCompileManager.compileReport(FRA_BalanceSheetTable3SubReport);
                JasperDesign FRA_CashFlow_CashFromInvestingActivitiesSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_CashFlow_CashFromInvestingActivities.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_CashFlow_CashFromInvestingActivitiesSubReport = JasperCompileManager.compileReport(FRA_CashFlow_CashFromInvestingActivitiesSubReport);
                JasperDesign FRA_CashFlowStatement_CashFromOperatingActivitiesSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_CashFlowStatement_CashFromOperatingActivities.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_CashFlowStatement_CashFromOperatingActivitiesSubReport = JasperCompileManager.compileReport(FRA_CashFlowStatement_CashFromOperatingActivitiesSubReport);
                JasperDesign FRA_CashFlowStatement_GeneralSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_CashFlowStatement_General.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_CashFlowStatement_GeneralSubReport = JasperCompileManager.compileReport(FRA_CashFlowStatement_GeneralSubReport);
                JasperDesign FRA_DetailedPeerGroupComparisonTableSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_DetailedPeerGroupComparisonTable.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_DetailedPeerGroupComparisonTableSubReport = JasperCompileManager.compileReport(FRA_DetailedPeerGroupComparisonTableSubReport);
                JasperDesign FRA_DriverRatingTableSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_DriverRatingTable.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_DriverRatingTableSubReport = JasperCompileManager.compileReport(FRA_DriverRatingTableSubReport);
                JasperDesign FRA_GScoreSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_GScore.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_GScoreSubReport = JasperCompileManager.compileReport(FRA_GScoreSubReport);
                JasperDesign FRA_GScoreChartSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_GScoreChart.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_GScoreChartSubReport = JasperCompileManager.compileReport(FRA_GScoreChartSubReport);
                JasperDesign FRA_IncomeStatement_NonOperatingExpenseSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_IncomeStatement_NonOperatingExpense.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_IncomeStatement_NonOperatingExpenseSubReport = JasperCompileManager.compileReport(FRA_IncomeStatement_NonOperatingExpenseSubReport);
                JasperDesign FRA_IncomeStatement_OperatingExpenseSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_IncomeStatement_OperatingExpense.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_IncomeStatement_OperatingExpenseSubReport = JasperCompileManager.compileReport(FRA_IncomeStatement_OperatingExpenseSubReport);
                JasperDesign FRA_IncomeStatement_PerShareItemSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_IncomeStatement_PerShareItem.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_IncomeStatement_PerShareItemSubReport = JasperCompileManager.compileReport(FRA_IncomeStatement_PerShareItemSubReport);
                JasperDesign FRA_IncomeStatement_SupplementItemsSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_IncomeStatement_SupplementItems.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_IncomeStatement_SupplementItemsSubReport = JasperCompileManager.compileReport(FRA_IncomeStatement_SupplementItemsSubReport);
                JasperDesign FRA_IncomeStatementGeneralSectionSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_IncomeStatementGeneralSection.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_IncomeStatementGeneralSectionSubReport = JasperCompileManager.compileReport(FRA_IncomeStatementGeneralSectionSubReport);
                JasperDesign FRA_IncomeStatementRevenueSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_IncomeStatementRevenue.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_IncomeStatementRevenueSubReport = JasperCompileManager.compileReport(FRA_IncomeStatementRevenueSubReport);
                JasperDesign FRA_KeyFinancialRatioSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_KeyFinancialRatio.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_KeyFinancialRatioSubReport = JasperCompileManager.compileReport(FRA_KeyFinancialRatioSubReport);
                JasperDesign FRA_KeyFinancialRatioGraphsSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_KeyFinancialRatioGraphs.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_KeyFinancialRatioGraphsSubReport = JasperCompileManager.compileReport(FRA_KeyFinancialRatioGraphsSubReport);
                JasperDesign FRA_MarketViewSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_MarketView.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_MarketViewSubReport = JasperCompileManager.compileReport(FRA_MarketViewSubReport);
                JasperDesign FRA_PeerAnalysisSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_PeerAnalysis.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_PeerAnalysisSubReport = JasperCompileManager.compileReport(FRA_PeerAnalysisSubReport);
                JasperDesign FRA_PeerAnalysis_SectorSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_PeerAnalysis_Sector.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_PeerAnalysis_SectorSubReport = JasperCompileManager.compileReport(FRA_PeerAnalysis_SectorSubReport);
                JasperDesign FRA_PeerAnalysisDetailedIndustryTableSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_PeerAnalysisDetailedIndustryTable.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_PeerAnalysisDetailedIndustryTableSubReport = JasperCompileManager.compileReport(FRA_PeerAnalysisDetailedIndustryTableSubReport);
                JasperDesign FRA_PeerAnalysisIndustrySubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_PeerAnalysisIndustry.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_PeerAnalysisIndustrySubReport = JasperCompileManager.compileReport(FRA_PeerAnalysisIndustrySubReport);
                JasperDesign FRA_PeerAnalysisIndustryGraphSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_PeerAnalysisIndustryGraph.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_PeerAnalysisIndustryGraphSubReport = JasperCompileManager.compileReport(FRA_PeerAnalysisIndustryGraphSubReport);
                JasperDesign FRA_PeerAnalysisSectorGraphSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRA_PeerAnalysisSectorGraph.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRA_PeerAnalysisSectorGraphSubReport = JasperCompileManager.compileReport(FRA_PeerAnalysisSectorGraphSubReport);
                //
                JasperDesign FRACashFlow_CashFromFinancingActivitiesSubReport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRACashFlow_CashFromFinancingActivities.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRACashFlow_CashFromFinancingActivitiesSubReport = JasperCompileManager.compileReport(FRACashFlow_CashFromFinancingActivitiesSubReport);
                JasperDesign FRADriversSubreport = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRADriversSubreport.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRADriversSubreport = JasperCompileManager.compileReport(FRADriversSubreport);
                JasperDesign FRAKeyFinancialRatioSummaryTable = JRXmlLoader.load(getClass().getResourceAsStream("/JasperSources/FRAKeyFinancialRatioSummaryTable.jrxml"));
//			            //Compile Main Report
                JasperReport jasperFRAKeyFinancialRatioSummaryTable = JasperCompileManager.compileReport(FRAKeyFinancialRatioSummaryTable);

                // JasperReport jasperReport = JasperCompileManager.compileReport(getClass().getResourceAsStream("D://Raghenderdata//Codebase//RaghavChanges//TPRMWEB//src/main//resources//JasperSources//FRA_GScore.jrxml"));
                //Add parameters EntityId and Subreportdirectories
                Map<String, Object> parameters;
                parameters = new HashMap<String, Object>();
                parameters.put("CompanyId", companyId);
                parameters.put("CurrentYear", currentYear);
                parameters.put("LastYear", lastYear);
                parameters.put("SecondLastYear", secondLastYear);
                parameters.put("CalculationInterval", calculationInterval);
                parameters.put("IntegratorName", integratorName);
                parameters.put("FRAKEYHIGHLIGHTS_SUBREPORT", jasperKeyHighlightSubReport);
                parameters.put("BalanceSheet_CurrentLiabilitiesTable", jasperBalanceSheet_CurrentLiabilitiesTableSubReport);
                parameters.put("BalanceSheetNonCurrentLiabilitiesTable", jasperBalanceSheetNonCurrentLiabilitiesTableSubReport);
                parameters.put("FRA_BalanceSheetCurrentAssetDataset", jasperFRA_BalanceSheetCurrentAssetDatasetSubReport);
                parameters.put("FRA_BalanceSheetNonCurrentAssetTable", jasperFRA_BalanceSheetNonCurrentAssetTableSubReport);
                parameters.put("FRA_BalanceSheetShareHolderEquity", jasperFRA_BalanceSheetShareHolderEquitySubReport);
                parameters.put("FRA_BalanceSheetSupplementItem", jasperFRA_BalanceSheetSupplementItemSubReport);
                parameters.put("FRA_BalanceSheetTable1", jasperFRA_BalanceSheetTable1SubReport);
//			           // parameters.put("FRA_BalanceSheetTable3", jasperFRA_BalanceSheetTable3SubReport);
                parameters.put("FRA_CashFlow_CashFromInvestingActivities", jasperFRA_CashFlow_CashFromInvestingActivitiesSubReport);
                parameters.put("FRA_CashFlowStatement_CashFromOperatingActivities", jasperFRA_CashFlowStatement_CashFromOperatingActivitiesSubReport);
                parameters.put("FRA_CashFlowStatement_General", jasperFRA_CashFlowStatement_GeneralSubReport);
                parameters.put("FRA_DetailedPeerGroupComparisonTable", jasperFRA_DetailedPeerGroupComparisonTableSubReport);
                parameters.put("FRA_DriverRatingTable", jasperFRA_DriverRatingTableSubReport);
                parameters.put("FRA_GScore", jasperFRA_GScoreSubReport);
                parameters.put("FRA_GScoreChart", jasperFRA_GScoreChartSubReport);
                parameters.put("FRA_IncomeStatement_NonOperatingExpense", jasperFRA_IncomeStatement_NonOperatingExpenseSubReport);
                parameters.put("FRA_IncomeStatement_OperatingExpense", jasperFRA_IncomeStatement_OperatingExpenseSubReport);
                parameters.put("FRA_IncomeStatement_PerShareItem", jasperFRA_IncomeStatement_PerShareItemSubReport);
                parameters.put("FRA_IncomeStatement_SupplementItems", jasperFRA_IncomeStatement_SupplementItemsSubReport);
                parameters.put("FRA_IncomeStatementGeneralSection", jasperFRA_IncomeStatementGeneralSectionSubReport);
                parameters.put("FRA_IncomeStatementRevenue", jasperFRA_IncomeStatementRevenueSubReport);
                parameters.put("FRA_KeyFinancialRatio", jasperFRA_KeyFinancialRatioSubReport);
                parameters.put("FRA_KeyFinancialRatioGraphs", jasperFRA_KeyFinancialRatioGraphsSubReport);
                parameters.put("FRA_MarketView", jasperFRA_MarketViewSubReport);
                parameters.put("FRA_PeerAnalysis", jasperFRA_PeerAnalysisSubReport);
                parameters.put("FRA_PeerAnalysis_Sector", jasperFRA_PeerAnalysis_SectorSubReport);
                parameters.put("FRA_PeerAnalysisDetailedIndustryTable", jasperFRA_PeerAnalysisDetailedIndustryTableSubReport);
                parameters.put("FRA_PeerAnalysisIndustry", jasperFRA_PeerAnalysisIndustrySubReport);
                parameters.put("FRA_PeerAnalysisIndustryGraph", jasperFRA_PeerAnalysisIndustryGraphSubReport);
                parameters.put("FRA_PeerAnalysisSectorGraph", jasperFRA_PeerAnalysisSectorGraphSubReport);
                parameters.put("FRACashFlow_CashFromFinancingActivities", jasperFRACashFlow_CashFromFinancingActivitiesSubReport);
                parameters.put("FRADriversSubreport", jasperFRADriversSubreport);
                parameters.put("FRAKeyFinancialRatioSummaryTable", jasperFRAKeyFinancialRatioSummaryTable);
                parameters.put("HEADERIMAGE", ReadingProperty.HEADERIMAGE);
                parameters.put("ALLLIGHT", ReadingProperty.ALLLIGHT);
                parameters.put("UPARROW", ReadingProperty.UPARROW);
                parameters.put("DOWNARROW", ReadingProperty.DOWNARROW);
                parameters.put("GENPACTLOGOFOOTER", ReadingProperty.GENPACTLOGOFOOTER);
                parameters.put("CopyRightFooter", ReadingProperty.CopyRightFooter);
                parameters.put("NOCHANGESYMBOL", ReadingProperty.NOCHANGESYMBOL);
                parameters.put("GSCORECHARTIMAGE", ReadingProperty.GSCORECHARTIMAGE);
                parameters.put("POSITIVEHIGHLIGHTLOGO", ReadingProperty.POSITIVEHIGHLIGHTLOGO);
                parameters.put("NEGATIVEHIGHLIGHTLOGO", ReadingProperty.NEGATIVEHIGHLIGHTLOGO);

                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReportMain, parameters, connection);

               
                pdfArray = JasperExportManager.exportReportToPdf(jasperPrint);
                //export(jasperPrint,outputDir);
                System.out.println(jasperReportMain);
                System.out.println(pdfArray.length);
            } catch (JRException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        return pdfArray;
    }

    public String gettingYear(String currentYear, int i) {
        String resultantYear = null;
        String[] yearArray = currentYear.split("-");
        yearArray[0] = String.valueOf((Integer.valueOf(yearArray[0]) - i));
        yearArray[1] = String.valueOf((Integer.valueOf(yearArray[1]) - i));

        resultantYear = yearArray[0] + "-" + yearArray[1];

        return resultantYear;
    }

}
